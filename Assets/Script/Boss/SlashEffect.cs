using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashEffect : MonoBehaviour
{
    public new ParticleSystem particleSystem;
    // Start is called before the first frame update
    void Start()
    {
        particleSystem = gameObject.GetComponent<ParticleSystem>();
    }

    public void Play(float positionX, float direction)
    {
        gameObject.transform.localScale = new Vector3(-direction, 1, 1);
        gameObject.transform.position = new Vector3(positionX, transform.position.y);
        particleSystem.Play();
    }
}
