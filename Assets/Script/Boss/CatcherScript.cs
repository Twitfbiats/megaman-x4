using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatcherScript : MonoBehaviour
{
    public bool catched = false;
    private void OnCollisionEnter2D(Collision2D other) 
    {
        Debug.Log(other.collider.gameObject.name);
        if (other.collider.gameObject.name.Equals("Scythe")) catched = true;
    }
}
